import {ExtraOptions, RouterModule, Routes} from '@angular/router';
import {NgModule} from '@angular/core';


const routes: Routes = [
    {
        path: 'home',
        loadChildren: './home/home.module#HomeModule',
    }, {
        path: 'houses',
        loadChildren: './houses/houses.module#HousesModule',
    }, {
        path: 'flats',
        loadChildren: './flats/flats.module#FlatsModule',
    },
    {path: '', redirectTo: 'home', pathMatch: 'full'},
    {path: '**', redirectTo: ''},
];

const config: ExtraOptions = {
    useHash: false,
};

@NgModule({
    imports: [RouterModule.forRoot(routes, config)],
    exports: [RouterModule],
})
export class AppRoutingModule {
}
