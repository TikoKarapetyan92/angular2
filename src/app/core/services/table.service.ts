import {Injectable} from '@angular/core';
import {of as observableOf, Observable} from 'rxjs';

export class Table {
    id: number;
    beds: number;
    sqm: number;
    floor: number;
    price: number;
    status: number | string;
}

@Injectable()
export class TableService {

    private dataA: Table[] = [{
        id: 1,
        beds: 2,
        sqm: 126,
        floor: 4,
        price: 200.500,
        status: 1,
    }, {
        id: 2,
        beds: 2,
        sqm: 126,
        floor: 4,
        price: 200.500,
        status: 0,
    }, {
        id: 3,
        beds: 2,
        sqm: 126,
        floor: 4,
        price: 200.500,
        status: 1,
    }, {
        id: 4,
        beds: 2,
        sqm: 126,
        floor: 4,
        price: 200.500,
        status: 0,
    }, {
        id: 5,
        beds: 2,
        sqm: 126,
        floor: 4,
        price: 200.500,
        status: 0,
    }, {
        id: 6,
        beds: 2,
        sqm: 126,
        floor: 4,
        price: 200.500,
        status: 0,
    }, {
        id: 7,
        beds: 2,
        sqm: 126,
        floor: 4,
        price: 200.500,
        status: 0,
    }, {
        id: 8,
        beds: 2,
        sqm: 126,
        floor: 4,
        price: 200.500,
        status: 0,
    }];

    private dataB: Table[] = [{
        id: 1,
        beds: 2,
        sqm: 126,
        floor: 4,
        price: 200.500,
        status: 1,
    }, {
        id: 2,
        beds: 2,
        sqm: 126,
        floor: 4,
        price: 200.500,
        status: 1,
    }, {
        id: 3,
        beds: 2,
        sqm: 126,
        floor: 4,
        price: 200.500,
        status: 0,
    }, {
        id: 4,
        beds: 2,
        sqm: 126,
        floor: 4,
        price: 200.500,
        status: 0,
    }, {
        id: 5,
        beds: 2,
        sqm: 126,
        floor: 4,
        price: 200.500,
        status: 0,
    }, {
        id: 6,
        beds: 2,
        sqm: 126,
        floor: 4,
        price: 200.500,
        status: 0,
    }, {
        id: 7,
        beds: 2,
        sqm: 126,
        floor: 4,
        price: 200.500,
        status: 0,
    }, {
        id: 8,
        beds: 2,
        sqm: 126,
        floor: 4,
        price: 200.500,
        status: 0,
    }];


    getDataA(): Observable<Table[]> {
        return observableOf(this.dataA);
    }

    getDataB(): Observable<Table[]> {
        return observableOf(this.dataB);
    }
}
