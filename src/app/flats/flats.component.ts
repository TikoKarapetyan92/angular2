import {Component, OnDestroy, OnInit} from '@angular/core';
import {Table, TableService} from '../core/services/table.service';
import {Subscription} from 'rxjs';

@Component({
    selector: 'app-flats',
    templateUrl: './flats.component.html',
    styleUrls: ['./flats.component.sass']
})
export class FlatsComponent implements OnInit, OnDestroy {

    dataA: Table[];
    dataB: Table[];
    thableASubscription: Subscription;
    thableBSubscription: Subscription;

    constructor(private tableData: TableService) {
    }

    ngOnInit() {

        this.thableASubscription = this.tableData.getDataA()
            .subscribe((dataA: Table[]) => {
                console.log(dataA);
                this.dataA = dataA;
            });
        this.thableBSubscription = this.tableData.getDataB()
            .subscribe((dataB: Table[]) => {
                console.log(dataB);
                this.dataB = dataB;
            });
    }

    ngOnDestroy() {
        this.thableASubscription.unsubscribe();
        this.thableBSubscription.unsubscribe();
    }

}
