import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {FlatsRoutingModule} from './flats-routing.module';
import {FlatsComponent} from './flats.component';
import {TableService} from '../core/services/table.service';

@NgModule({
    declarations: [FlatsComponent],
    imports: [
        CommonModule,
        FlatsRoutingModule
    ],
    providers: [
        TableService
    ]
})
export class FlatsModule {
}
