export const MENU_ITEMS = [
    {
        path: 'pages',
        children: [
            {
                path: 'risk-overview',
                role: 'Home',
                data: {
                    menu: {
                        title: 'Home',
                        // icon: '../../../../assets/img/dashboard.png',
                        selected: false,
                        expanded: false,
                        order: 0
                    }
                }
            }
        ]
    }
];
