import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import {PagesComponent} from './pages.component';

const routes: Routes = [{
    path: '',
    component: PagesComponent,
    children: [
        {
        path: 'home',
        loadChildren: './home/home.module#HomeModule',
        }, {
            path: 'houses',
            loadChildren: './houses/houses.module#HousesModule',
        },
    ],
}];
@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class PagesRoutingModule {
}
