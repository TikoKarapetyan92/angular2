import { Component } from '@angular/core';
import { MENU_ITEMS } from './pages-menu';

@Component({
    selector: 'app-pages',
    styleUrls: ['pages.component.sass'],
    template: `
        <div class="al-main">
            <div class="al-content">
                <nav>
                    <a routerLink="home">Home Component</a>
                    <a routerLink="houses">Lazy Module</a>
                </nav>
                <router-outlet></router-outlet>

            </div>
        </div>
        <footer class="al-footer clearfix">
            <div class="al-footer-right" translate><i class="ion-heart"></i></div>
            <div class="al-footer-main clearfix">
                <div class="al-copy">&copy; <a href="" translate>test</a> 2019</div>
                <ul class="al-share clearfix">
                </ul>
            </div>
        </footer>
  `,
})
export class PagesComponent {

    menu = MENU_ITEMS;
    constructor() {
        // console.log(563);
    }
}