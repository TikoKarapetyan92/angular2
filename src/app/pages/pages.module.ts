import { NgModule } from '@angular/core';

import { PagesComponent } from './pages.component';
import { PagesRoutingModule } from './pages-routing.module';
import { HousesComponent } from '../houses/houses.component';
import { FlatsComponent } from '../flats/flats.component';


const PAGES_COMPONENTS = [
    PagesComponent,
];

@NgModule({
    imports: [
        PagesRoutingModule,
    ],
    declarations: [
        PAGES_COMPONENTS,
    ],
})
export class PagesModule {}